﻿using System;
using System.Text;
using System.Threading;

namespace Machine_à_café
{
    class Program
    {
        static void Main()
        {
            // Claude Dubé
            // DA: 7353179

            // Éviter les fautes d’orthographe dans les textes affichés par la console
            Console.OutputEncoding = Encoding.UTF8;

            // DÉCLARATIONS ET INITIALISATIONS
            // Par convention, les noms de variables commencent part une lettre minuscule
            // et les noms de constantes sont toutes en majuscules.
            // Donc RENDU a été remplacé par rendu.
            // Note : Le programme tel qu’il est conçu avec une liste de prix toujours arrondis au dollar près
            //        n’exige pas l’utilisation de nombres réels. On aurait pu tout aussi bien se contenter d’entiers.
            //        Voir à ce propos les remarques sur la validation du montant.
            int choix = 0;
            float prix = 0f;
            float rendu = 0f; 
            float monnaie = 0f;

            // MENU DE LA MACHINE À CAFÉ
            Console.WriteLine(" _________________________________");
            Console.WriteLine("|         MACHINE À CAFÉ          |");
            Console.WriteLine("|---------------------------------|");
            Console.WriteLine("|          --> MENU <--           |");
            Console.WriteLine("|---------------------------------|");
            Console.WriteLine("| [1]    : EXPRESSO               |");
            Console.WriteLine("| [2]    : DOUBLE EXPRESSO        |");
            Console.WriteLine("| [3]    : RISTRETTO              |");
            Console.WriteLine("| [4]    : LONG                   |");
            Console.WriteLine("| [5]    : CAFÉ MACCHIATO         |");
            Console.WriteLine("| [6]    : CAFÉ CRÈME             |");
            Console.WriteLine("| [7]    : CAFÉ NOISETTE          |");
            Console.WriteLine("| [8]    : CAPPUCCINO             |");
            Console.WriteLine("| [9]    : CAPPUCCINO ALLÉGÉ      |");
            Console.WriteLine("| [10]   : CAFÉ AMÉRICAIN         |");
            Console.WriteLine("| [11]   : CAFÉ GLACÉ             |");
            Console.WriteLine("| [12]   : CAFÉ BREVE             |");
            Console.WriteLine("| [13]   : MOCHA BREVE            |");
            Console.WriteLine("| [14]   : MOCHA                  |");
            Console.WriteLine("| [15]   : OEIL AU BEURRE NOIR    |");
            Console.WriteLine("| [16]   : CAFÉ LATTE             |");
            Console.WriteLine("| [17]   : CAFÉ CON LECHE         |");
            Console.WriteLine("|---------------------------------|");
            Console.WriteLine("| Entrez votre choix              |");
            Console.WriteLine("|_________________________________|");
            Console.Write(" ---> ");

            // ENTRÉES : LE CHOIX DE L’UTILISATEUR
            // Si choix ne se lit pas comme un entier
            // (compris entre les valeurs minimale et maximale sur le système d’exploitation),
            // alors la valeur 0 est affectée à la variable choix. 
            int.TryParse(Console.ReadLine(), out choix);

            // VALIDATION DU CHOIX DE L’UTILISATEUR
            if (choix < 1 || 17 < choix)
            {
                // CHOIX INVALIDE: MESSAGE D’ERREUR ET SORTIE
                Console.WriteLine("VOTRE CHOIX EST INVALIDE !");
                Console.WriteLine("Veuillez refaire votre choix.");
                Console.ReadKey();
                return;
            }

            // AFFICHER LE CAFÉ CHOISI ET LE PRIX
            switch (choix)
            {
                case 1:
                    prix = 2f;
                    Console.WriteLine($"EXPRESSO : {prix} $");
                    break;
                case 2:
                    prix = 4f;
                    Console.WriteLine($"DOUBLE EXPRESSO : {prix} $");
                    break;
                case 3:
                    prix = 2f;
                    Console.WriteLine($"RISTRETTO : {prix} $");
                    break;
                case 4:
                    prix = 5f;
                    Console.WriteLine($"LONG : {prix} $");
                    break;
                case 5:
                    prix = 5f;
                    Console.WriteLine($"CAFÉ MACCHIATO : {prix} $");
                    break;
                case 6:
                    prix = 6f;
                    Console.WriteLine($"CAFÉ CRÈME : {prix} $");
                    break;
                case 7:
                    prix = 6f;
                    Console.WriteLine($"CAFÉ NOISETTE : {prix} $");
                    break;
                case 8:
                    prix = 5f;
                    Console.WriteLine($"CAPPUCCINO : {prix} $");
                    break;
                case 9:
                    prix = 5f;
                    Console.WriteLine($"CAPPUCCINO ALLÉGÉ : {prix} $");
                    break;
                case 10:
                    prix = 5f;
                    Console.WriteLine($"CAFÉ AMÉRICAIN : {prix} $");
                    break;
                case 11:
                    prix = 4f;
                    Console.WriteLine($"CAFÉ GLACÉ : {prix} $");
                    break;
                case 12:
                    prix = 5f;
                    Console.WriteLine($"CAFÉ BREVE : {prix} $");
                    break;
                case 13:
                    prix = 7f;
                    Console.WriteLine($"MOCHA BREVE : {prix} $");
                    break;
                case 14:
                    prix = 6f;
                    Console.WriteLine($"MOCHA : {prix} $");
                    break;
                case 15:
                    prix = 5f;
                    Console.WriteLine($"OEIL AU BEURRE NOIR : {prix} $");
                    break;
                case 16:
                    prix = 5f;
                    Console.WriteLine($"CAFÉ LATTE : {prix} $");
                    break;
                case 17:
                    prix = 7f;
                    Console.WriteLine($"CAFÉ CON LECHE : {prix} $");
                    break;
            }

            // GUIDAGE DE L’USAGER: PAIEMENT
            Console.WriteLine("Entrez la monnaie ");
            Console.Write(" ---> ");

            // ENTRÉES : LA MONNAIE
            // Si monnaie ne se lit pas comme un nombre réel
            // (compris entre les valeurs minimale et maximale sur le système d’exploitation),
            // alors la valeur 0 est affectée à la variable monnaie. 
            float.TryParse(Console.ReadLine(), out monnaie);

            // VALIDATION DU CHOIX DE L’UTILISATEUR
            if (monnaie < prix || 20f < monnaie)
            {
                // CHOIX INVALIDE: MESSAGE D’ERREUR ET SORTIE
                // On n’a le droit d’insérer qu’une seule fois la monnaie : 1$, 2$, 5$, etc. Les sous ne sont pas pris en compte.
                // Note : Pas besoin de vérifier si l’utilisateur a payé avec des pièces de 1 $,
                //        car il ne peut insérer qu’une seule fois la monnaie et le prix d’un café commence à partir de 2 $.
                // Note : De plus, s’il tente d’introduire une pièce différente de 2 $, elle sera rejetée mécaniquement.
                //        Il est donc inutile de traiter les pièces de monnaie inférieures à 2 $ puisque le programme ne les verra pas.
                // Note : Le programme n’a pas besoin de vérifier que l’utilisateur n’a pas inséré un billet de 3 $, 4 $, 6 $, etc.
                //        car ces billets n’existent pas !
                // Note : On pourrait ajouter une limite de 20 $ aux paiements recevables, car c’est une distributrice de café,
                //        et non une machine à échanger de gros billets contre de petites coupures ce, pour le prix d’une boisson chaude !
                //        Mais il y a fort à parier que scanneur utilisé rejettera tout billet supérieur à 20 $.
                //        Le modèle de luxe qui prend n'importe quel billet, c'est pour les guichets automatiques, pas pour les distributrices.
                //        Donc encore là, le programme ne les verra pas.
                Console.WriteLine("MONTANT PERÇU INVALIDE !");
                Console.WriteLine("Veuillez prendre votre argent et refaire votre dépôt.");
                // Signaler à l’usager qu’un maximum de 20 $ s’applique est futile dans ce contexte, même au nom du guidage de l’usager.
                Console.ReadKey();
                return;
            }

            // CHOIX VALIDE: CALCUL ET AFFECTATION DE LA MONNAIE À RENDRE
            // /!\ Critique: Comment sait-on que la machine possède les liquidités nécessaire au remboursement ?
            rendu = monnaie - prix;

            // PRÉPARATION CAFÉ EN 10 SECONDES
            // Il faut cumuler les temps de pause pour faire 10 secondes
            // 10 = 1 + 1 + 1 + 7
            // Note : Les 10 secondes peuvent paraître longues lorsqu’on teste le code.
            //        Par contre, on sait qu’un utilisateur entend les bruits de la machine à café et
            //        que même une personne malentendante touchera la machine et sentira les vibrations.
            //        Donc l’utilisateur saura que son café est en préparation. Cela me semble acceptable.
            //        Une barre de progression ou un sablier pourrait être ajouté si les tests révèlent
            //        que les usagers ont tendance à croire que la machine est défectueuse.
            Console.Clear();
            Console.Write("Préparation du café en cours");
            Thread.Sleep(1000);
            Console.Write(".");
            Thread.Sleep(1000);
            Console.Write(".");
            Thread.Sleep(1000);
            Console.WriteLine(".");
            Thread.Sleep(7000);

            // AFFICHER LA MONNAIE RENDUE ET SERVIR CAFÉ
            Console.WriteLine();
            Console.WriteLine(@"ARGENT RENDU : " + rendu + " $");
            Console.WriteLine(@" __________________");
            Console.WriteLine(@"|Prenez votre Café |");
            Console.WriteLine(@" ******************");

            // AFFICHER L’IMAGE DU CAFÉ CHOISI
            // La rétroaction avec l’image et le nom du café choisi permet à l’utilisateur
            // de valider son choix à postériori.
            switch (choix)
            {
                // La solution suivante conviendra à un client qui aime offrir de beaux dessins.
                // Surtout s’il possède la rigueur nécessaire pour maintenir le code ou s’il 
                // dispose de fournisseurs capables d’effectuer ces mises à jour.
                // Le code se répète, par contre il permet de voir l’image résultante.

                case 1:
                    // IMAGE EXPRESSO
                    Console.WriteLine(" ( ( (");
                    Console.WriteLine("  ) ) )");
                    Console.WriteLine("|        |");
                    Console.WriteLine(@"|EXPRESSO|\");
                    Console.WriteLine("|________|/");
                    break;

                case 2:
                    // IMAGE DOUBLE EXPRESSO
                    Console.WriteLine(" ( ( (");
                    Console.WriteLine("  ) ) )");
                    Console.WriteLine("|        |");
                    Console.WriteLine(@"|DOUBLE  |\");
                    Console.WriteLine("|EXPRESSO|/");
                    Console.WriteLine("|________|");
                    break;

                case 3:
                    // IMAGE RISTRETTO
                    Console.WriteLine(" ( ( (");
                    Console.WriteLine("  ) ) )");
                    Console.WriteLine("|         |");
                    Console.WriteLine(@"|RISTRETTO|\");
                    Console.WriteLine("|_________|/");
                    break;

                case 4:
                    // IMAGE LONG
                    Console.WriteLine(" ( ( (");
                    Console.WriteLine("  ) ) )");
                    Console.WriteLine("|        |");
                    Console.WriteLine("|        |");
                    Console.WriteLine(@"|  LONG  |\");
                    Console.WriteLine("|        |/");
                    Console.WriteLine("|________|");
                    break;

                case 5:
                    // IMAGE CAFÉ MACCHIATO
                    Console.WriteLine(" ( ( (");
                    Console.WriteLine("  ) ) )");
                    Console.WriteLine("|         |");
                    Console.WriteLine(@"|CAFÉ     |\");
                    Console.WriteLine("|MACCHIATO|/");
                    Console.WriteLine("|_________|");
                    break;

                case 6:
                    // IMAGE CAFÉ CRÈME
                    Console.WriteLine(" ( ( (");
                    Console.WriteLine("  ) ) )");
                    Console.WriteLine("|        |");
                    Console.WriteLine("| CAFÉ   |");
                    Console.WriteLine(@"| CRÈME  |\");
                    Console.WriteLine("|        |/");
                    Console.WriteLine("|________|");
                    break;

                case 7:
                    // IMAGE CAFÉ NOISETTE
                    Console.WriteLine(" ( ( (");
                    Console.WriteLine("  ) ) )");
                    Console.WriteLine("|        |");
                    Console.WriteLine("|CAFÉ    |");
                    Console.WriteLine(@"|NOISETTE|\");
                    Console.WriteLine("|        |/");
                    Console.WriteLine("|________|");
                    break;

                case 8:
                    // IMAGE CAPPUCCINO
                    Console.WriteLine(" ( ( ( ( (");
                    Console.WriteLine("  ) ) ) ) )       ");
                    Console.WriteLine("|                |__");
                    Console.WriteLine(@"|                |  \");
                    Console.WriteLine("|  CAPPUCINO     |  /");
                    Console.WriteLine("|                |_/");
                    Console.WriteLine(@"\                /");
                    Console.WriteLine(@" \______________/");
                    break;

                case 9:
                    // IMAGE CAPPUCCINO ALLÉGÉ
                    Console.WriteLine(" ( ( ( ( (");
                    Console.WriteLine("  ) ) ) ) )       ");
                    Console.WriteLine("|                |__");
                    Console.WriteLine(@"|                |  \");
                    Console.WriteLine("|  CAPPUCINO     |  /");
                    Console.WriteLine("|  ALLÉGÉ        |_/");
                    Console.WriteLine(@"\                /");
                    Console.WriteLine(@" \______________/");
                    break;

                case 10:
                    // IMAGE CAFÉ AMÉRICAIN
                    Console.WriteLine(" ( ( ( ( (");
                    Console.WriteLine("  ) ) ) ) )       ");
                    Console.WriteLine("|                |__");
                    Console.WriteLine("|  CAFÉ          |  |");
                    Console.WriteLine("|  AMÉRICAIN     |_/");
                    Console.WriteLine(@"\                /");
                    Console.WriteLine(@" \______________/");
                    break;

                case 11:
                    // IMAGE CAFÉ GLACÉ
                    Console.WriteLine(" ( ( (");
                    Console.WriteLine("  ) ) )  ");
                    Console.WriteLine("|        |");
                    Console.WriteLine("| CAFÉ   |");
                    Console.WriteLine(@"| GLACÉ  |\");
                    Console.WriteLine("|        |/");
                    Console.WriteLine("|________|");
                    break;

                case 12:
                    // IMAGE CAFÉ BREVE
                    Console.WriteLine(" ( ( ( ( (");
                    Console.WriteLine("  ) ) ) ) )       ");
                    Console.WriteLine("|                |__");
                    Console.WriteLine("|                |  |");
                    Console.WriteLine("|  CAFÉ BREVE    |_/");
                    Console.WriteLine(@"\                /");
                    Console.WriteLine(@" \______________/");
                    break;

                case 13:
                    // IMAGE MOCHA BREVE
                    Console.WriteLine(" ( ( ( ( (");
                    Console.WriteLine("  ) ) ) ) )       ");
                    Console.WriteLine("|                |__");
                    Console.WriteLine(@"|                |  \");
                    Console.WriteLine("|  MOCHA BREVE   |  /");
                    Console.WriteLine("|                |_/");
                    Console.WriteLine(@"\                /");
                    Console.WriteLine(@" \______________/");
                    break;

                case 14:
                    // IMAGE MOCHA
                    Console.WriteLine(" ( ( ( ( (");
                    Console.WriteLine("  ) ) ) ) )       ");
                    Console.WriteLine("|                |__");
                    Console.WriteLine("|                |  |");
                    Console.WriteLine("|  MOCHA         |_/");
                    Console.WriteLine(@"\                /");
                    Console.WriteLine(@" \______________/");
                    break;

                case 15:
                    // IMAGE OEIL AU BEURRE NOIR
                    Console.WriteLine(" ( ( ( ( (");
                    Console.WriteLine("  ) ) ) ) )       ");
                    Console.WriteLine("|                |__");
                    Console.WriteLine(@"|                |  \");
                    Console.WriteLine("|  OEIL AU       |  /");
                    Console.WriteLine("|  BEURRE NOIR   |_/");
                    Console.WriteLine(@"\                /");
                    Console.WriteLine(@" \______________/");
                    break;

                case 16:
                    // IMAGE CAFÉ LATTE
                    Console.WriteLine(" ( ( ( ( ( ( (");
                    Console.WriteLine("  ) ) ) ) ) ) )     ");
                    Console.WriteLine("|                   |__");
                    Console.WriteLine(@"|                   |  \");
                    Console.WriteLine("|  CAFÉ LATTE       |  /");
                    Console.WriteLine("|                   |_/");
                    Console.WriteLine(@"\                  /");
                    Console.WriteLine(@" \________________/");
                    break;

                case 17:
                    // IMAGE CAFÉ CON LECHE
                    Console.WriteLine(" ( ( ( ( ( ( (");
                    Console.WriteLine("  ) ) ) ) ) ) )     ");
                    Console.WriteLine("|                   |__");
                    Console.WriteLine(@"|                   |  \");
                    Console.WriteLine("|  CAFÉ CON LECHE   |  /");
                    Console.WriteLine("|                   |_/");
                    Console.WriteLine(@"\                  /");
                    Console.WriteLine(@" \________________/");
                    break;

            }

            // FIN ACHAT
            Console.WriteLine();
            Console.WriteLine("À LA PROCHAINE !");
            Console.ReadKey();
                        
        }

    }
}
